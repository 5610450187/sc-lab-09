package Model;
import java.util.Comparator;


public class EarningComparator implements Comparator<Company> {

	@Override
	public int compare(Company cmp1, Company cmp2) {
		if(cmp1.getIncome() < cmp2.getIncome()){return -1;}
		if (cmp1.getIncome() > cmp2.getIncome()){return 1;}
		return 0;
	}

}
