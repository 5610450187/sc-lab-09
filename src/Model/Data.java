package Model;

import Interface.Measurable;

public class Data {
	public static double average(Measurable[] objects) {
		double sum = 0;
		for (Measurable obj : objects) {
			sum += obj.getMeasure();
		}
		if (objects.length > 0) {
			return sum / objects.length;
		} else {
			return 0;
		}
	}


	public static Measurable min(Measurable m1, Measurable m2) {
		if (m1.getMeasure() < m2.getMeasure()) {
			return m1;
		} else {
			return m2;
		}
	}
}
