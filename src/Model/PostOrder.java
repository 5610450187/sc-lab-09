package Model;

import java.util.ArrayList;

import Interface.Traversal;

public class PostOrder implements Traversal {

	@Override
	public ArrayList<String> traverse(ArrayList<Object> node) {
		for (int i = 0; i < node.size(); i++) {
			Node n = (Node) node.get(i);
			if(n != null){
				n.getLeft();
				n.getRight();
				n.getValue();
				list.add(n+" ");
			}

		}
		return list;
	}

}
