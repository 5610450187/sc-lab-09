package Model;

import java.util.Comparator;

public class ProfitComparator implements Comparator<Company>{

	@Override
	public int compare(Company o1, Company o2) {
		return (int) (o1.getIncome()-o2.getExpenses());
	}

}
