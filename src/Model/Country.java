package Model;

import Interface.Measurable;

public class Country implements Measurable {
	private String name;
	private int population;

	public Country(String name, int population) {
		this.name = name;
		this.population = population;
	}

	public String getName() {
		return this.name;
	}

	public int getPopulation() {
		return this.population;
	}

	@Override
	public double getMeasure() {
		return this.getPopulation();
	}



}
