package Model;

import java.util.Comparator;

import Interface.Taxable;

public class TaxComparator implements Comparator<Taxable> {

	@Override
	public int compare(Taxable o1, Taxable o2) {
		if(o1.getTax() < o2.getTax()){return -1;}
		if (o1.getTax() > o2.getTax()){return 1;}
		return 0;
	}

}
