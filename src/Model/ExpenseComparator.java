package Model;

import java.util.Comparator;

public class ExpenseComparator implements Comparator<Company> {

	@Override
	public int compare(Company o1, Company o2) {
		if(o1.getExpenses() < o2.getExpenses()){ return -1;}
		if(o1.getExpenses() > o2.getExpenses()){ return 1;}
		return 0;
	}

}
