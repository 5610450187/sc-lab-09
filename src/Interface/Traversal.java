package Interface;

import java.util.ArrayList;

import Model.Node;

public interface Traversal {
	public ArrayList<String> list = new ArrayList<String>();
	public ArrayList<String> traverse(ArrayList<Object> node);
}
