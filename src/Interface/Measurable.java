package Interface;

public interface Measurable {
	public double getMeasure();
}
