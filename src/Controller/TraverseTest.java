package Controller;

import java.util.ArrayList;

import Model.InOrder;
import Model.Node;
import Model.PostOrder;
import Model.PreOrder;

public class TraverseTest {


	public static void main(String[] args) {
		ReportConsole r = new ReportConsole();
		ArrayList<Node> list = new ArrayList<Node>();
		Node a = new Node("A", new Node("B",new Node("D", null, null), null), new Node("C",null, null));
		r.display(a, new InOrder());
		System.out.println();
		r.display(a, new PreOrder());
		System.out.println();
		r.display(a, new PostOrder());

	}

}
