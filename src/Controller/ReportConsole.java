package Controller;


import java.util.ArrayList;

import Interface.Traversal;
import Model.Node;

public class ReportConsole {
	
	public void display(Node node,Traversal t){
		if(node != null){
			System.out.println("Value ::"+node.getValue());
			display(node.getLeft(), t);
			display(node.getRight(), t);
	
		}
	}

}
