package Controller;

import java.util.ArrayList;
import java.util.Collections;
import Interface.Taxable;
import Model.Company;
import Model.EarningComparator;
import Model.ExpenseComparator;
import Model.Person;
import Model.Product;
import Model.ProfitComparator;
import Model.TaxComparator;

public class Test {
	public static void main(String[] args) {
		Test t = new Test();
		t.testPerson();
		t.testProduct();
		t.testCompany();
		t.testQ4();
		
	}
	
	private void testPerson(){
		System.out.println("---------- Sort by YearlyIncome ----------");
		ArrayList<Person> persons = new ArrayList<Person>();
		persons.add(new Person("Rick", 185, 1080000));
		persons.add(new Person("Glenn", 180, 506500));
		persons.add(new Person("Carl", 175, 700000));
		Collections.sort(persons);
		for (Person p : persons) {
			System.out.println(p.toString());
		}
		System.out.println();
		
	}
	private void testProduct(){
		System.out.println("---------- Sort by Tax ----------");
		ArrayList<Product> products = new ArrayList<Product>();
		products.add(new Product("Mobile", 25000));
		products.add(new Product("Computer", 58000));
		products.add(new Product("Watch", 15000));
		Collections.sort(products);
		for (Product p : products) {
			System.out.println(p.toString());
		}
		System.out.println();
	}
	private void testCompany(){
		
		ArrayList<Company> companies = new ArrayList<Company>();
		companies.add(new Company("Apple", 1000000, 800000));
		companies.add(new Company("Windows", 2000000, 500000));
		companies.add(new Company("Google", 4000000, 200000));
		
		Collections.sort(companies, new EarningComparator());
		System.out.println("---------- Sort by Earning ----------");
		for (Company company : companies) {
			System.out.println(company.toString());
		}
		System.out.println();
		
		Collections.sort(companies, new ExpenseComparator());
		System.out.println("---------- Sort by Expense ----------");
		for (Company company : companies) {
			System.out.println(company.toString());
		}
		System.out.println();
		
		Collections.sort(companies, new ProfitComparator());
		System.out.println("---------- Sort by Profit ----------");
		for (Company company : companies) {
			System.out.println(company.toString());
		}
		System.out.println();
	}
	
	private void testQ4(){
		System.out.println("---------- Sort by Tax ----------");
		ArrayList<Taxable> persons = new ArrayList<Taxable>();
		persons.add(new Person("Rick", 185, 100000));
		persons.add(new Person("Glenn", 180, 500000));
		
		ArrayList<Taxable> companies = new ArrayList<Taxable>();
		companies.add(new Company("Apple", 1000000, 800000));
		companies.add(new Company("Windows", 2000000, 500000));
		
		ArrayList<Taxable> products = new ArrayList<Taxable>();
		products.add(new Product("TV", 10000));
		products.add(new Product("Computer", 20000));
		
		ArrayList<Taxable> allElements = new ArrayList<Taxable>();
		allElements.addAll(persons);
		allElements.addAll(companies);
		allElements.addAll(products);
		Collections.sort(allElements, new TaxComparator());
		
		for (Taxable taxable : allElements) {
			System.out.println(taxable.toString());
		}
	}
}
